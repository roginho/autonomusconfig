/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusconfig.classes;

/**
 *
 * @author Roger
 */
public class Configuracao {

    private ConfiguracaoGeral configuracaoGeral;
    private ConfiguracaoEmail configuracaoEmail;

    public ConfiguracaoGeral getConfiguracaoGeral() {
        return configuracaoGeral;
    }

    public void setConfiguracaoGeral(ConfiguracaoGeral configuracaoGeral) {
        this.configuracaoGeral = configuracaoGeral;
    }

    public ConfiguracaoEmail getConfiguracaoEmail() {
        return configuracaoEmail;
    }

    public void setConfiguracaoEmail(ConfiguracaoEmail configuracaoEmail) {
        this.configuracaoEmail = configuracaoEmail;
    }

}
