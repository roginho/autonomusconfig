package br.com.autonomusconfig.utils;

import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableRowSorter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author roger
 */
public class Singleton {

    private Singleton() {
    }

    private static class SingletonHolder {

        public static final Singleton instance = new Singleton();
    }

    public static Singleton getInstance() {
        return SingletonHolder.instance;
    }

    public void passaCamposComEnter(Component campo) {
        // Colocando enter para pular de campo  
        HashSet conj = new HashSet(campo.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
        conj.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
        campo.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, conj);

        //pra invocar  
        //new passaCamposComEnter(panelCampos);  
    }

    public String[] Computador() {
        String info[] = new String[2];
        try {
            InetAddress inet = InetAddress.getLocalHost();
            info[0] = inet.getHostAddress();
            info[1] = inet.getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
        }
        return info;
    }

    public void mascaraFone(JCheckBox j, JFormattedTextField campo) {
        if (j.isSelected()) {
            try {
                MaskFormatter mascaraFone = new MaskFormatter("(##)#####-####");
                campo.setFormatterFactory(new DefaultFormatterFactory(mascaraFone));
                campo.setValue(null);
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao validar telefone");
            }
        } else {
            try {
                MaskFormatter mascaraFone = new MaskFormatter("(##)####-####");
                campo.setFormatterFactory(new DefaultFormatterFactory(mascaraFone));
                campo.setValue(null);
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao validar telefone");

            }
        }
    }

    public static boolean soContemNumeros(String texto) {
        return texto.matches("[0-9]");
    }

    public String limpaMascaraFone(String fone) {
        fone = fone.replace('(', ' ');
        fone = fone.replace(')', ' ');
        fone = fone.replace('-', ' ');
        return fone;
    }

    public void filtroSorter(TableRowSorter sorter, String text, int coluna) {
        if (text.length() == 0) {
            sorter.setRowFilter(null);
        } else {
            sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text, coluna));
        }
    }

    public void copyFile(File source, File destination) throws IOException {
        if (destination.exists()) {
            destination.delete();
        }
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }

    public void abrirSite(String site) {
        try {
            try {
                java.awt.Desktop.getDesktop().browse(new java.net.URI(site));
            } catch (URISyntaxException ex) {
                Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean temNumeros(String texto) {
        for (int i = 0; i < texto.length(); i++) {
            if (Character.isDigit(texto.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public File buscarDiretorio() {
        File diretorio = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int res = chooser.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            diretorio = chooser.getSelectedFile();
        }
        return diretorio;
    }

    public String buscarArquivoImagem() {
        String arquivo = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & PNG Images", new String[]{"jpg", "png"});
        chooser.setFileFilter(filter);
        int res = chooser.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            arquivo = chooser.getSelectedFile().getAbsolutePath();
        }
        return arquivo;
    }

    public ImageIcon redimensionaImg(File image, int new_w, int new_h) {
        try {
            BufferedImage imagem = ImageIO.read(image);
            BufferedImage new_img = new BufferedImage(new_w, new_h, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = new_img.createGraphics();
            g.drawImage(imagem, 0, 0, new_w, new_h, null);
            g.dispose();
            return new ImageIcon(new_img);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //adicionar, alterar, excluir
    public String iconeJTable(String opcao) {
        String caminho = "";
        if (opcao.equalsIgnoreCase("alterar")) {
            caminho = "iconeAterar16x16.png";
        } else if (opcao.equalsIgnoreCase("excluir")) {
            caminho = "iconeExcluir16x16.png";
        } else if (opcao.equalsIgnoreCase("adicionar")) {
            caminho = "iconeAdicionar16x16.png";
        }
        return diretorioImagens() + caminho;
    }

    public String diretorioImagens() {
        return "/br/com/roger/gui/imagem/";
    }

    public double converteStringParaDouble(String valor) {
        double value = 0;
        if (valor != null && !valor.trim().isEmpty()) {
            value = Double.parseDouble(valor.replace(",", "."));
        }
        return value;
    }

    public String converteDoubleParaString(Double valor, int casasDecimais) {

        String resultado = "";
        if (valor != null) {
            resultado = String.format("%." + casasDecimais + "f", valor);
            resultado = resultado.replace(".", ",");
        }
        return resultado;
    }

    public int convertStringToInt(String num) {
        int numero = 0;
        try {
            numero = Integer.parseInt(num);
        } catch (NumberFormatException e) {
            System.out.println("parse value is not valid : " + e);
        }
        return numero;
    }

    public Date converteStringParaData(String data) {
        if (data == null || data.equals("")) {
            return null;
        }
        Date date = null;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = (java.util.Date) formatter.parse(data);
        } catch (ParseException ex) {
            Logger.getLogger(Singleton.class.getName()).log(Level.SEVERE, null, ex);
        }

        return date;
    }

    public String converteDataParaString(Date data) {
        String dt = "";
        if (data != null) {
            SimpleDateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
            dt = formataData.format(data);
        }
        return dt;
    }

    public String converteDataParaStringMysql(Date data) {
        String dt = "";
        if (data != null) {
            SimpleDateFormat formataData = new SimpleDateFormat("yyyy/MM/dd");
            dt = formataData.format(data);
        }
        return dt;
    }

    public Date retornaPrimeiroDiaMes(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public Date retornaUltimoDiaMes(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public Date somaMes(Date d, int soma) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + soma);
        return cal.getTime();
    }

    public String caminhoSistema() {
        return System.getProperty("user.dir");
    }

    public String caminhoLogo() {
        return System.getProperty("user.dir") + "//logo.png";
    }

    public String caminhoMysqldump() {
        return System.getProperty("user.dir") + "\\mysqldump.exe";
    }

    public boolean confirma(String msg) {
        int result = JOptionPane.showConfirmDialog(null, msg, "", JOptionPane.YES_NO_OPTION);
        return result == JOptionPane.YES_OPTION;
    }

    public String geraNumerosLetrasAleatorios(int tam) {
        int qtdeMaximaCaracteres = tam;
        String[] caracteres = {"a", "1", "b", "2", "4", "5", "6", "7", "8",
            "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
            "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z"};
        StringBuilder senha = new StringBuilder();
        for (int i = 0; i < qtdeMaximaCaracteres; i++) {
            int posicao = (int) (Math.random() * caracteres.length);
            senha.append(caracteres[posicao]);
        }
        return senha.toString();
    }

    public String geraSenhaAleatoriaCriptografada(String senhaAleatoria, int tamanhoSenha) {
        Date dt = new Date();
        String data = Singleton.getInstance().converteDataParaStringMysql(dt);
        String hash256 = SHACheckSum.hash256(senhaAleatoria + data);
        System.out.println("" + senhaAleatoria + "" + data);
        String senha = hash256.substring(0, tamanhoSenha);
        return senha;
    }
}
