/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.autonomusconfig.utils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

public class SendMail {

    private final String host;
    private final String from;
    private final String user;
    private final String pass;
    private final int port;
    private final String to;
    private final String subject;
    private final String text;
    private final File file;

    public SendMail(String host, String remetente, String usuario, String senha, int porta, String destinatario, String assunto, String msg, File anexo) {
        this.host = host;
        this.from = remetente;
        this.user = usuario;
        this.pass = senha;
        this.port = porta;
        this.to = destinatario;
        this.subject = assunto;
        this.text = msg;
        this.file = anexo;
    }

    public void enviarEmail() throws MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", host);
        // props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.port", ""+port);
        props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.user", user);
        props.put("mail.debug", "true");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        });
//        try {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);
        message.setText(text);

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        Multipart multipart = new MimeMultipart();
        messageBodyPart = new MimeBodyPart();
        messageBodyPart.attachFile(file);

        DataSource source = new FileDataSource(file);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(file.getName());
        multipart.addBodyPart(messageBodyPart);
        message.setContent(multipart);
        System.out.println("Enviando..." + from + " host: " + host + " port:" + port);

        try (Transport tr = session.getTransport("smtp")) {
            tr.connect(host, user, pass);
            message.saveChanges();      // don't forget this
            tr.sendMessage(message, message.getAllRecipients());
            System.out.println("Email enviado com sucesso.");
            JOptionPane.showMessageDialog(null, "Email enviado com sucesso.");

        }
    }
}
