package br.com.autonomusconfig.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Roger
 */
public class Arquivos implements Serializable {

    private boolean test = false;
    private File arquivo = null;

    public void gravaDados(Object obj, String arq) {
        test = arquivo.exists();
        try {
            if (!test) {
                arquivo.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(arq, true);
            ObjectOutputStream writer = new ObjectOutputStream(fos);
            writer.writeObject(obj);
            writer.flush();
            writer.close();
            fos.flush();
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(Arquivos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Object leDados() {
        test = arquivo.exists();
        Object obj = null;
        if (!test) {
            System.out.println("Arquivo não existe");
        } else {
            try {
                FileInputStream fis = new FileInputStream(arquivo);
                ObjectInputStream reader = new ObjectInputStream(fis);
                obj = (Object) reader.readObject(); 
                reader.close();
                fis.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Arquivos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Arquivos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Arquivos.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return obj;
    }

    public Arquivos(File arq) {
        this.arquivo = arq;
    }

    public static void main(String[] args) {
        // TODO code application logic here
    }

}
