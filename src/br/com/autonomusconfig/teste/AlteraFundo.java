package br.com.autonomusconfig.teste;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Roger
 */
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.JDesktopPane;

public class AlteraFundo extends JDesktopPane {

    Image img;

    public AlteraFundo(String caminho) {
        File imagem = new File(caminho);
        try {
            img = javax.imageio.ImageIO.read(imagem);
        } catch (Exception e) {
        }//do nothing  
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (img != null) {
            g.drawImage(img, 0, 0, this);
        } else {
            g.drawString("", 50, 50);
        }
    }
}
