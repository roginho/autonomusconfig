package br.com.autonomusconfig;

import br.com.autonomusconfig.classes.Configuracao;
import br.com.autonomusconfig.classes.ConfiguracaoEmail;
import br.com.autonomusconfig.utils.SendMail;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.mail.MessagingException;

/**
 *
 * @author Róger
 */
public class AutonomusConfig {

    private String config = "config.xml";

    private static class SingletonHolder {

        public static final AutonomusConfig instance = new AutonomusConfig();
    }

    public static AutonomusConfig getInstance() {
        return SingletonHolder.instance;
    }

    public AutonomusConfig() {
        this.config = System.getProperty("user.dir") + "/" + this.config;
    }

    public boolean existeArquivoConfig() {
        File f = new File(this.config);
        return f.exists();
    }

    public String getCaminhoConfigXml() {
        return config;
    }

    public Configuracao getArquivoConfiguracao() {
        Configuracao config = null;
        if (this.existeArquivoConfig()) {
            try {
                XStream xStream = new XStream(new DomDriver());
                xStream.processAnnotations(Configuracao.class);
                xStream.alias("configuracao", Configuracao.class);
                BufferedReader input = new BufferedReader(new FileReader(this.getCaminhoConfigXml()));
                config = (Configuracao) xStream.fromXML(input);
                input.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return config;
    }
    
    public void envioEmail(String dest, String assunto, String texto, File anexo) throws MessagingException, IOException {
        Configuracao configuracao = AutonomusConfig.getInstance().getArquivoConfiguracao();
        if (configuracao.getConfiguracaoEmail() != null) {
            ConfiguracaoEmail configuracaoEmail = configuracao.getConfiguracaoEmail();
            String host = configuracaoEmail.getSmtp();
            String user = configuracaoEmail.getUsuario();
            String emailEnvio = configuracaoEmail.getRemetente();
            String pass = configuracaoEmail.getSenha();
            int port = configuracaoEmail.getPorta();
            SendMail s = new SendMail(host, emailEnvio, user, pass, port, dest, assunto, texto, anexo);
            s.enviarEmail();
        }
    }
}
