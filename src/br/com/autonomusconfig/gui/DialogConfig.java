package br.com.autonomusconfig.gui;

import br.com.autonomusconfig.AutonomusConfig;
import br.com.autonomusconfig.classes.Configuracao;
import br.com.autonomusconfig.classes.ConfiguracaoEmail;
import br.com.autonomusconfig.classes.ConfiguracaoGeral;
import br.com.autonomusconfig.utils.SelectAllTheThings;
import br.com.autonomusconfig.utils.Singleton;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Róger
 */ 
public class DialogConfig extends javax.swing.JDialog {

    private void lerXML() {
        AutonomusConfig conf = new AutonomusConfig();
        Configuracao xml = conf.getArquivoConfiguracao();
        if (xml != null) {
            ConfiguracaoGeral geral = xml.getConfiguracaoGeral();
            if (geral != null) {
                textFieldIpServidor.setText(geral.getIpServidor());
                textFieldPorta.setText(geral.getPorta());
                textFieldNomeBanco.setText(geral.getNomeBanco());
                if (geral.getLocalInstalacao() != null) {
                    textFieldLocalInstalacao.setText(geral.getLocalInstalacao());
                } else {
                    textFieldLocalInstalacao.setText(System.getProperty("user.dir"));
                }
            }
            ConfiguracaoEmail email = xml.getConfiguracaoEmail();
            if (email != null) {
                if (email.getRemetente() != null) {
                    textFieldEmail.setText(email.getRemetente());
                }
                if (email.getUsuario() != null) {
                    textFieldUsuario.setText(email.getUsuario());
                }
                if (email.getSenha() != null) {
                    PasswordFieldSenha.setText(email.getSenha());
                }
                if (email.getSmtp() != null) {
                    textFieldSmtp.setText(email.getSmtp());
                }
                if (email.getPorta() != null) {
                    textFieldPortaEmail.setText(email.getPorta() + "");
                }
            }
        }
    }

    private void salvar() {
        Configuracao conf = new Configuracao();
        if (AutonomusConfig.getInstance().getArquivoConfiguracao() != null) {
            conf = AutonomusConfig.getInstance().getArquivoConfiguracao();
        }
        ConfiguracaoGeral confGeral = new ConfiguracaoGeral();
        ConfiguracaoEmail confEmail = new ConfiguracaoEmail();
        XStream xStream = new XStream(new DomDriver());
        xStream.processAnnotations(Configuracao.class);
        xStream.alias("configuracao", Configuracao.class);
        xStream.processAnnotations(ConfiguracaoGeral.class);
        xStream.alias("configuracaoGeral", ConfiguracaoGeral.class);
        xStream.processAnnotations(ConfiguracaoEmail.class);
        xStream.alias("configuracaoEmail", ConfiguracaoGeral.class);
        confGeral.setIpServidor(textFieldIpServidor.getText());
        confGeral.setPorta(textFieldPorta.getText());
        confGeral.setNomeBanco(textFieldNomeBanco.getText());
        confGeral.setLocalInstalacao(textFieldLocalInstalacao.getText());
        confEmail.setRemetente(textFieldEmail.getText());
        confEmail.setUsuario(textFieldUsuario.getText());
        char[] senhaCripto = PasswordFieldSenha.getPassword();
        String senha = new String(senhaCripto);
        confEmail.setSenha(senha);
        confEmail.setPorta(Singleton.getInstance().convertStringToInt(textFieldPortaEmail.getText()));
        confEmail.setSmtp(textFieldSmtp.getText());
        conf.setConfiguracaoEmail(confEmail);
        conf.setConfiguracaoGeral(confGeral);

        String toXML = xStream.toXML(conf);
        PrintWriter pw;
        try {
            AutonomusConfig c = new AutonomusConfig();

            pw = new PrintWriter(new FileWriter(c.getCaminhoConfigXml()));
            pw.println(toXML);
            pw.close();
            this.dispose();
        } catch (IOException ex) {
            Logger.getLogger(DialogConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DialogConfig(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Singleton.getInstance().passaCamposComEnter(panelConf);
        SelectAllTheThings.addListeners(panelConf);
        lerXML();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelConf = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textFieldIpServidor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textFieldPorta = new javax.swing.JTextField();
        textFieldNomeBanco = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textFieldLocalInstalacao = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        textFieldEmail = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        textFieldPortaEmail = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        textFieldSmtp = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        textFieldUsuario = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        PasswordFieldSenha = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Configurações");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/autonomusconfig/imagens/iconeGravar32x32.png"))); // NOI18N
        jButton1.setText("Salvar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        panelConf.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuração"));

        jLabel1.setText("Ip Servidor");

        jLabel2.setText("Porta:");

        textFieldNomeBanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldNomeBancoActionPerformed(evt);
            }
        });

        jLabel3.setText("Base de dados:");

        jLabel4.setText("Local Instalação:");

        javax.swing.GroupLayout panelConfLayout = new javax.swing.GroupLayout(panelConf);
        panelConf.setLayout(panelConfLayout);
        panelConfLayout.setHorizontalGroup(
            panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(4, 4, 4)
                .addGroup(panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldIpServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldLocalInstalacao, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldNomeBanco, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelConfLayout.setVerticalGroup(
            panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel1)
                    .addComponent(textFieldIpServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel2)
                    .addComponent(textFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel3)
                    .addComponent(textFieldNomeBanco, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel4)
                    .addComponent(textFieldLocalInstalacao, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Geral", panelConf);

        jLabel5.setText("Email:");

        jLabel6.setText("Porta:");

        jLabel7.setText("Smtp:");

        textFieldSmtp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldSmtpActionPerformed(evt);
            }
        });

        jLabel8.setText("Senha:");

        jLabel9.setText("Usuário:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldEmail)
                    .addComponent(textFieldUsuario)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textFieldPortaEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PasswordFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 188, Short.MAX_VALUE))
                    .addComponent(textFieldSmtp))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel5)
                    .addComponent(textFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(PasswordFieldSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel6)
                    .addComponent(textFieldPortaEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel7)
                    .addComponent(textFieldSmtp, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Email", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        salvar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void textFieldNomeBancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldNomeBancoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldNomeBancoActionPerformed

    private void textFieldSmtpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldSmtpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldSmtpActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField PasswordFieldSenha;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel panelConf;
    private javax.swing.JTextField textFieldEmail;
    private javax.swing.JTextField textFieldIpServidor;
    private javax.swing.JTextField textFieldLocalInstalacao;
    private javax.swing.JTextField textFieldNomeBanco;
    private javax.swing.JTextField textFieldPorta;
    private javax.swing.JTextField textFieldPortaEmail;
    private javax.swing.JTextField textFieldSmtp;
    private javax.swing.JTextField textFieldUsuario;
    // End of variables declaration//GEN-END:variables
}
